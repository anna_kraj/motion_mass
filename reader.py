import csv


class CSVReader:
    filepath = ''
    delimiter = ''

    def __init__(self, filepath, delimiter=','):
        self.filepath = filepath
        self.delimiter = delimiter

    def read(self):  # to be validated, probably need 2D dictionary
        with open(self.filepath, 'r') as csv_file:
            reader = csv.reader(csv_file, delimiter=self.delimiter)
            dict_list = []
            for row in reader:
                dict_list.append(row)
        return dict_list
