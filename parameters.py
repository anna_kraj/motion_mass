import numpy as np
from numpy.linalg import norm

import draw
import kinect
from kinect import JointType


def _calculate_motion_mass(x_list, y_list, z_list, timestamps):
    time_deltas = _calculate_diffs(timestamps)
    distances = _calculate_distances(x_list, y_list, z_list)
    velocities = _divide_params(distances, time_deltas)
    accelerations = _divide_diffs(velocities, time_deltas)
    jerks = _divide_diffs(accelerations, time_deltas)
    return {"time": sum(time_deltas),
            "trajectory": sum(distances),
            "velocity": sum(velocities),
            "acceleration": sum(accelerations),
            "jerk": sum(jerks)}


def _calculate_distances(x, y, z):
    x_diffs = _calculate_diffs(x)
    y_diffs = _calculate_diffs(y)
    z_diffs = _calculate_diffs(z)
    x_squared = np.array(x_diffs) ** 2
    y_squared = np.array(y_diffs) ** 2
    z_squared = np.array(z_diffs) ** 2
    result = (x_squared + y_squared + z_squared) ** 0.5
    return result


def _calculate_diffs(params):
    return [abs(params[i] - params[i - 1]) for i in range(1, len(params))]


def _divide_diffs(params, params2):
    diffs = _calculate_diffs(params)
    divided = _divide_params(np.array(diffs), np.array(params2)[:len(diffs)])
    return divided


def _divide_params(params, params2):
    np.seterr(divide='ignore', invalid='ignore')
    divided = np.divide(np.array(params), np.array(params2))
    divided[~ np.isfinite(divided)] = 0
    return divided


def _calculate_dict_average(dict1, dict2):
    avg = {}
    for key in dict1:
        avg[key] = np.average(dict1[key] + dict2[key])
    return avg


def _get_list_slice_to_index_incl(list_to_slice, index):
    return list_to_slice[:index + 1]


def _get_list_slice_from_index_incl(list_to_slice, index):
    return list_to_slice[index:]


def _convert_coordinates_relative_to_step_start(bodies):
    converted = []
    spine_mid = bodies[0].joints[JointType.SpineMid]
    for body in bodies:
        converted.append(kinect.Body.with_relative_coordinates_to_origin(body, spine_mid))
    return converted


def calculate_parameters(bodies):
    result = {"eucl_distance": {}, "eucl_div_trajectory": {}, "eucl_div_acceleration": {}, "eucl_div_leg_length": {}}
    leg_length = 0
    # bodies = _convert_coordinates_relative_to_step_start(bodies)
    for body in bodies:
        lengths = []
        length = body.calc_legs_length()
        if length is not None:
            lengths.append(length)
        if len(lengths) > 0:
            leg_length = sum(lengths) / len(lengths)
    for joint_type in bodies[0].joints.keys():
        # if joint_type == JointType.SpineMid:
        #     continue
        x = []
        y = []
        z = []
        t = []
        for i in range(len(bodies)):
            joint = bodies[i].joints[joint_type]
            x.append(joint.x)
            y.append(joint.y)
            z.append(joint.z)
            t.append(joint.timestamp)
        params = _calculate_motion_mass(x, y, z, t)
        for key, value in params.items():
            if key not in result.keys():
                result[key] = {}
            result[key][joint_type] = value
        eucl_dist = _calculate_distances([x[0], x[-1]], [y[0], y[-1]], [z[0], z[-1]])[0]
        result["eucl_distance"][joint_type] = eucl_dist
        result["eucl_div_trajectory"][joint_type] = eucl_dist / params["trajectory"]
        result["eucl_div_acceleration"][joint_type] = eucl_dist / params["acceleration"]
        result["eucl_div_leg_length"][joint_type] = eucl_dist / leg_length
    return result


def calculate_angle(joint1, joint2, joint3):
    v1 = np.array([joint1.x - joint2.x, joint1.y - joint2.y, joint1.z - joint2.z])
    v2 = np.array([joint3.x - joint2.x, joint3.y - joint2.y, joint3.z - joint2.z])
    c = np.dot(v1, v2) / norm(v1) / norm(v2)
    angle = np.rad2deg(np.arccos(np.clip(c, -1, 1)))
    return angle


def calculate_angles(bodies):
    result = {"max": {}, "min": {}, "delta": {}, "first": {}, "last": {}}
    angle_labels = ["right_knee", "left_knee", "right_hip", "left_hip", "knee_hip_knee",
                    "head_spine_shoulder_spine_mid", "spine_shoulder_spine_mid_spine_base",
                    "head_spine_shoulder_spine_base"]
    right_knee_angles = []
    left_knee_angles = []
    right_hip_angles = []
    left_hip_angles = []
    knee_hip_knee_angles = []
    spine1_angles = []
    spine2_angles = []
    spine3_angles = []
    for body in bodies:
        # draw.draw_body_points(body)

        spine_base = body.joints[JointType.SpineBase]
        spine_mid = body.joints[JointType.SpineMid]
        spine_shoulder = body.joints[JointType.SpineShoulder]
        head = body.joints[JointType.Head]

        right_hip = body.joints[JointType.HipRight]
        left_hip = body.joints[JointType.HipLeft]

        right_knee = body.joints[JointType.KneeRight]
        left_knee = body.joints[JointType.KneeLeft]

        right_ankle = body.joints[JointType.AnkleRight]
        left_ankle = body.joints[JointType.AnkleLeft]

        right_knee_angles.append(calculate_angle(right_hip, right_knee, right_ankle))
        left_knee_angles.append(calculate_angle(left_hip, left_knee, left_ankle))
        right_hip_angles.append(calculate_angle(head, spine_base, right_knee))
        left_hip_angles.append(calculate_angle(head, spine_base, left_knee))
        knee_hip_knee_angles.append(calculate_angle(right_knee, spine_base, left_knee))
        spine1_angles.append(calculate_angle(head, spine_shoulder, spine_mid))
        spine2_angles.append(calculate_angle(spine_shoulder, spine_mid, spine_base))
        spine3_angles.append(calculate_angle(head, spine_shoulder, spine_base))

    max_angle = max(right_knee_angles)
    min_angle = min(right_knee_angles)
    result["max"][angle_labels[0]] = max_angle
    result["min"][angle_labels[0]] = min_angle
    result["delta"][angle_labels[0]] = max_angle - min_angle
    result["first"][angle_labels[0]] = right_knee_angles[0]
    result["last"][angle_labels[0]] = right_knee_angles[-1]

    max_angle = max(left_knee_angles)
    min_angle = min(left_knee_angles)
    result["max"][angle_labels[1]] = max_angle
    result["min"][angle_labels[1]] = min_angle
    result["delta"][angle_labels[1]] = max_angle - min_angle
    result["first"][angle_labels[1]] = left_knee_angles[0]
    result["last"][angle_labels[1]] = left_knee_angles[-1]

    max_angle = max(right_hip_angles)
    min_angle = min(right_hip_angles)
    result["max"][angle_labels[2]] = max_angle
    result["min"][angle_labels[2]] = min_angle
    result["delta"][angle_labels[2]] = max_angle - min_angle
    result["first"][angle_labels[2]] = right_hip_angles[0]
    result["last"][angle_labels[2]] = right_hip_angles[-1]

    max_angle = max(left_hip_angles)
    min_angle = min(left_hip_angles)
    result["max"][angle_labels[3]] = max_angle
    result["min"][angle_labels[3]] = min_angle
    result["delta"][angle_labels[3]] = max_angle - min_angle
    result["first"][angle_labels[3]] = left_hip_angles[0]
    result["last"][angle_labels[3]] = left_hip_angles[-1]

    max_angle = max(knee_hip_knee_angles)
    min_angle = min(knee_hip_knee_angles)
    result["max"][angle_labels[4]] = max_angle
    result["min"][angle_labels[4]] = min_angle
    result["delta"][angle_labels[4]] = max_angle - min_angle
    result["first"][angle_labels[4]] = knee_hip_knee_angles[0]
    result["last"][angle_labels[4]] = knee_hip_knee_angles[-1]

    max_angle = max(spine1_angles)
    min_angle = min(spine1_angles)
    result["max"][angle_labels[5]] = max_angle
    result["min"][angle_labels[5]] = min_angle
    result["delta"][angle_labels[5]] = max_angle - min_angle
    result["first"][angle_labels[5]] = spine1_angles[0]
    result["last"][angle_labels[5]] = spine1_angles[-1]

    max_angle = max(spine2_angles)
    min_angle = min(spine2_angles)
    result["max"][angle_labels[6]] = max_angle
    result["min"][angle_labels[6]] = min_angle
    result["delta"][angle_labels[6]] = max_angle - min_angle
    result["first"][angle_labels[6]] = spine2_angles[0]
    result["last"][angle_labels[6]] = spine2_angles[-1]

    max_angle = max(spine3_angles)
    min_angle = min(spine3_angles)
    result["max"][angle_labels[7]] = max_angle
    result["min"][angle_labels[7]] = min_angle
    result["delta"][angle_labels[7]] = max_angle - min_angle
    result["first"][angle_labels[7]] = spine3_angles[0]
    result["last"][angle_labels[7]] = spine3_angles[-1]
    return result


def smooth_using_moving_average(frame_size, data):
    points_sum = 0
    avg_points = []
    for i in range(0, len(data) - frame_size + 1):
        inner_counter = 0
        index = i
        while inner_counter < frame_size:
            points_sum += data[index]
            inner_counter += 1
            index += 1
        avg_points.append(points_sum / frame_size)
        points_sum = 0
    return avg_points
