import os
import sys

import numpy as np
import pandas

from experiment import separate_walks
from kinect import KinectVersion
from parameters import calculate_parameters, calculate_angles
from reader import CSVReader


def write_matrix_to_file(result, save_to_file):
    data_frame = pandas.DataFrame(result)
    data_frame.to_csv(save_to_file)


def write_data_to_file(result, save_to_file):
    data_frame = pandas.DataFrame(result)
    if not os.path.exists(save_to_file):
        data_frame.to_csv(save_to_file)
    else:
        with open(save_to_file, 'a') as csvfile:
            data_frame.to_csv(csvfile, header=False)


def main(dirpath_to_read_from, dirpath_to_write_to, filename, column):
    filepath = os.path.join(dirpath_to_read_from, filename)
    data = CSVReader(filepath).read()
    walks = separate_walks(data, column, KinectVersion.FIRST)
    params_by_steps = {}
    angles_by_steps = {}
    for i in range(len(walks)):
        walk = walks[i]
        steps = walk.detect_steps()
        for step in steps:
            angles_for_step = calculate_angles(step)
            params_for_step = calculate_parameters(step)
            for mm_param, params in params_for_step.items():
                if mm_param not in params_by_steps.keys():
                    params_by_steps[mm_param] = {}
                for joint_type, value in params.items():
                    if joint_type not in params_by_steps[mm_param].keys():
                        params_by_steps[mm_param][joint_type] = []
                    params_by_steps[mm_param][joint_type].append(value)

            for angle_param, params in angles_for_step.items():
                if angle_param not in angles_by_steps.keys():
                    angles_by_steps[angle_param] = {}
                for joint_type, value in params.items():
                    if joint_type not in angles_by_steps[angle_param].keys():
                        angles_by_steps[angle_param][joint_type] = []
                    angles_by_steps[angle_param][joint_type].append(value)
            head, tail = os.path.split(dirpath)
            # print(os.path.join(dirpath_to_write_to, tail + "_angles_steps_week.csv"))
            # print(os.path.join(dirpath_to_write_to, tail + "_mm_steps_week.csv"))
            write_data_to_file(angles_for_step, os.path.join(dirpath_to_write_to, tail + "_angles_steps_week.csv"))
            write_data_to_file(params_for_step, os.path.join(dirpath_to_write_to, tail + "_mm_steps_week.csv"))
            head, tail = os.path.split(dirpath_to_write_to)
            # print(os.path.join(head, filename.split('.')[0] + "_angles_steps.csv"))
            # print(os.path.join(head, filename.split('.')[0] + "_mm_steps.csv"))
            write_data_to_file(angles_for_step, os.path.join(head, filename.split('.')[0] + "_angles_steps.csv"))
            write_data_to_file(params_for_step, os.path.join(head, filename.split('.')[0] + "_mm_steps.csv"))
    averages = {}
    for angle_param, params in params_by_steps.items():
        averages[angle_param] = {}
        for joint_type, value in params.items():
            average = np.average(params_by_steps[angle_param][joint_type])
            # params_by_steps[angle_param][joint_type].append(average)
            averages[angle_param][joint_type] = average
    angle_averages = {}
    for angle_param, params in angles_by_steps.items():
        angle_averages[angle_param] = {}
        for joint_type, value in params.items():
            average = np.average(angles_by_steps[angle_param][joint_type])
            # angles_by_steps[angle_param][joint_type].append(average)
            angle_averages[angle_param][joint_type] = average
    if len(averages.keys()) > 0:
        # print(os.path.join(dirpath_to_write_to, filename.split('.')[0] + "_mm_avg" + ".csv"))
        write_matrix_to_file(averages, os.path.join(dirpath_to_write_to, filename.split('.')[0] + "_mm_avg" + ".csv"))
    if len(angle_averages.keys()) > 0:
        # print(os.path.join(dirpath_to_write_to, filename.split('.')[0] + "_angles_avg" + ".csv"))
        write_matrix_to_file(angle_averages,
                             os.path.join(dirpath_to_write_to, filename.split('.')[0] + "_angles_avg" + ".csv"))


if __name__ == '__main__':
    rootdir = sys.argv[1]
    column = int(sys.argv[2])
    for (dirpath, _, filenames) in os.walk(rootdir):
        for filename in filenames:
            if ".csv" in filename:
                if 'Kondimised' in dirpath.split('/') and '_data.csv' in filename:
                    print(dirpath + '/' + filename)
                    head, tail = os.path.split(dirpath)
                    # head, tail = os.path.split(head)
                    # input('Press enter to continue: ')
                    main(dirpath, head, filename, column)
                # elif 'Alustamised' in dirpath.split('/') and '_data.csv' in filename:
                #     print(dirpath + '/' + filename)
                #     head, tail = os.path.split(dirpath)
                #     # head, tail = os.path.split(head)
                #     # input('Press enter to continue: ')
                #     main(dirpath, head, filename, column - 1)
