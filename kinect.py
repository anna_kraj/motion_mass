import math
from datetime import datetime, timedelta
from enum import Enum


class KinectVersion(Enum):
    FIRST = 1
    SECOND = 2


class JointType(Enum):
    SpineBase = 0
    SpineMid = 1
    Neck = 2
    Head = 3
    ShoulderLeft = 4
    ElbowLeft = 5
    WristLeft = 6
    HandLeft = 7
    ShoulderRight = 8
    ElbowRight = 9
    WristRight = 10
    HandRight = 11
    HipLeft = 12
    KneeLeft = 13
    AnkleLeft = 14
    FootLeft = 15
    HipRight = 16
    KneeRight = 17
    AnkleRight = 18
    FootRight = 19
    SpineShoulder = 20
    HandTipLeft = 21
    ThumbLeft = 22
    HandTipRight = 23
    ThumbRight = 24


class State(Enum):
    NOT_TRACKED = 0
    TRACKED = 1


class Joint:
    def __init__(self, x, y, z, timestamp, joint_type, state=State.TRACKED):
        self.x = x
        self.y = y
        self.z = z
        self.timestamp = timestamp
        self.joint_type = joint_type
        self.state = state

    def __str__(self):
        return '\t'.join([str(self.joint_type), str(self.timestamp), str(self.x), str(self.y), str(self.z)])


def get_seconds_from_date(date_time):
    pattern = "%Y-%m-%d %H:%M:%S.%f"
    milliseconds = (datetime.strptime(date_time, pattern) - datetime(1970, 1, 1)) // timedelta(milliseconds=1)
    return get_seconds(milliseconds)


def get_seconds(milliseconds):
    return float(milliseconds) / 1000


def distance_between_joints(joint1, joint2):
    return math.sqrt((joint1.y - joint2.y) ** 2 + (joint1.z + joint2.z) ** 2)


class Body:
    joint_types = [JointType.Head,
                   # JointType.Neck,
                   JointType.SpineShoulder,
                   JointType.SpineMid,
                   JointType.SpineBase,

                   JointType.ShoulderLeft,
                   JointType.ElbowLeft,
                   JointType.WristLeft,
                   JointType.HandLeft,
                   # JointType.ThumbLeft,
                   # JointType.HandTipLeft,

                   JointType.ShoulderRight,
                   JointType.ElbowRight,
                   JointType.WristRight,
                   JointType.HandRight,
                   # JointType.ThumbRight,
                   # JointType.HandTipRight,

                   JointType.HipLeft,
                   JointType.KneeLeft,
                   JointType.AnkleLeft,
                   JointType.FootLeft,

                   JointType.HipRight,
                   JointType.KneeRight,
                   JointType.AnkleRight,
                   JointType.FootRight]

    def __init__(self, joints, version):
        self.joints = joints
        self.version = version

    def __str__(self):
        result = ''
        for joint_type, joint in self.joints.items():
            result += str(joint_type) + ':\t\t' + str(joint) + '\n'
        return result

    @classmethod
    def from_raw_data(cls, raw, version=KinectVersion.SECOND):
        if version == KinectVersion.FIRST:
            timestamp = get_seconds(raw[1])
        else:
            timestamp = get_seconds_from_date(raw[1])
        joints = {}
        range_max = 77 if version == KinectVersion.SECOND else 62
        j = 0
        for i in range(2, range_max - 2):
            if i % 3 != 2:
                continue
            joint_type = Body.joint_types[j]
            j += 1
            try:
                x = float(raw[i])
                y = float(raw[i + 1])
                z = float(raw[i + 2])
                joints[joint_type] = Joint(x, y, z, timestamp, joint_type)
            except ValueError:
                joints[joint_type] = Joint(0, 0, 0, timestamp, joint_type, State.NOT_TRACKED)
        if version == KinectVersion.FIRST and len(joints) != 20:
            raise Exception
        elif version == KinectVersion.SECOND and len(joints) != 25:
            raise Exception
        return cls(joints, version)

    @classmethod
    def from_df_row(cls, row):
        timestamp = get_seconds(row[1])
        joints = {}
        j = 0
        for i in range(2, len(row.index), 3):
            joint_type = Body.joint_types[j]
            j += 1
            joints[joint_type] = Joint(row[i], row[i + 1], row[i + 2], timestamp, joint_type)
        return cls(joints, KinectVersion.FIRST)

    @classmethod
    def with_relative_coordinates(cls, body):
        spine_mid = body.joints[JointType.SpineMid]
        return cls.with_relative_coordinates_to_origin(body, spine_mid)

    @classmethod
    def with_relative_coordinates_to_origin(cls, body, joint):
        new_joints = {}
        origin_x = joint.x
        origin_y = joint.y
        origin_z = joint.z
        for joint_type, joint in body.joints.items():
            new_joints[joint_type] = Joint(joint.x - origin_x, joint.y - origin_y, joint.z - origin_z,
                                           joint.timestamp, joint_type, joint.state)
        return cls(new_joints, body.version)

    def calc_distance_between_ankles(self):
        left_ankle = self.joints[JointType.AnkleLeft]
        right_ankle = self.joints[JointType.AnkleRight]
        if left_ankle.state == State.NOT_TRACKED or right_ankle.state == State.NOT_TRACKED:
            return 0
        left_ankle_z = left_ankle.z
        right_ankle_z = right_ankle.z
        return left_ankle_z - right_ankle_z

    def calc_legs_length(self):
        left_ankle = self.joints[JointType.AnkleLeft]
        right_ankle = self.joints[JointType.AnkleRight]
        left_knee = self.joints[JointType.KneeLeft]
        right_knee = self.joints[JointType.KneeRight]
        left_hip = self.joints[JointType.HipLeft]
        right_hip = self.joints[JointType.HipRight]
        if left_ankle.state == State.NOT_TRACKED or right_ankle.state == State.NOT_TRACKED \
                or left_knee.state == State.NOT_TRACKED or right_knee.state == State.NOT_TRACKED \
                or left_hip.state == State.NOT_TRACKED or right_hip.state == State.NOT_TRACKED:
            return None
        left_leg = distance_between_joints(left_hip, left_knee) + distance_between_joints(left_knee, left_ankle)
        right_leg = distance_between_joints(right_hip, right_knee) + distance_between_joints(right_knee, right_ankle)
        return (left_leg + right_leg) / 2
