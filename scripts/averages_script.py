import sys
from os import walk

import numpy

first_try_filename = "average angles I kord.csv"
# first_try_filename = "averages I kord.csv"
second_try_filename = "average angles II kord.csv"
# second_try_filename = "averages II kord.csv"
general_averages = "average angles.csv"
# general_averages = "averages.csv"
delimiter = ','


def calc_avgs(matrix1, matrix2):
    result_matrix = []
    matrix_size = len(matrix1)
    matrix_line_size = len(matrix1[0])
    if len(matrix1) == len(matrix2) and len(matrix1[0]) == len(matrix2[0]):
        result_matrix.append(matrix1[0])
        for i in range(1, matrix_size):
            matrix_line = [matrix1[i][0]]
            for j in range(1, matrix_line_size):
                matrix_line.append((float(matrix1[i][j]) + float(matrix2[i][j])) / 2.0)
            result_matrix.append(matrix_line)
    else:
        raise Exception("Files sizes do not match")
    return result_matrix


def number_of_tokens_in_first_line(file_string):
    return len(file_string[0].split(delimiter))


def read_matrix_from_file(filename):
    matrix = []
    with open(filename) as data:
        for line in data:
            matrix_line = []
            for value in line.split(delimiter):
                matrix_line.append(value)
            matrix.append(matrix_line)
    return matrix


def write_matrix_to_file(result_matrix, dirpath):
    data = numpy.asarray(result_matrix)
    file_path = dirpath + "/" + general_averages
    numpy.savetxt(file_path, data, fmt="%s", delimiter=delimiter)
    return


rootdir = sys.argv[1]
f = []

for (dirpath, _, filenames) in walk(rootdir):
    if general_averages not in filenames:
        if first_try_filename and second_try_filename in filenames:
            matrix1 = read_matrix_from_file(dirpath + "/" + first_try_filename)
            matrix2 = read_matrix_from_file(dirpath + "/" + second_try_filename)
            result_matrix = calc_avgs(matrix1, matrix2)
            write_matrix_to_file(result_matrix, dirpath)
