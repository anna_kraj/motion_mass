import os
import sys

import numpy as np
import pandas

time_filenames = ["experiment_time.csv",
                  "experiment_time2.csv",
                  "experiment_time3.csv",
                  "forward_walking_time.csv",
                  "step_time.csv"]
delimiter = ','


def write_data_to_file(result, save_to_file):
    data_frame = pandas.DataFrame(result)
    if not os.path.exists(save_to_file):
        data_frame.to_csv(save_to_file, header=False, index=False)
    else:
        with open(save_to_file, 'a') as csvfile:
            data_frame.to_csv(csvfile, header=False, index=False)


def calculate_average(dirpath, filename):
    filepath = os.path.join(dirpath, filename)
    times = []
    with open(filepath) as data:
        for line in data:
            times.append(float(line))
    return np.mean(times)


rootdir = sys.argv[1]
f = []

for (dirpath, _, filenames) in os.walk(rootdir):
    for filename in filenames:
        if filename in time_filenames:
            print(dirpath, filename)
            avg = calculate_average(dirpath, filename)
            # print(avg)
            write_data_to_file([avg], os.path.join(dirpath, "avg_" + filename))
