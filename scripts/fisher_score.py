import os
import sys

import pandas
from numpy import array, average, zeros, unique, mean, std
from os import walk

from skfeature.function.similarity_based.fisher_score import fisher_score

averages_mm_filename = "nadal_2_avg_Kondimised_mm_steps_week.csv"
averages_mm_filename2 = "nadal_2_avg_Alustamised_mm_steps_week.csv"
averages_angles_filename = "nadal_2_avg_Kondimised_angles_steps_week.csv"
averages_mm_part = "_avg_Kondimised_mm_steps_week.csv"
averages_angles_part = "_avg_Kondimised_angles_steps_week.csv"
# general_averages = "average angles.csv"
random_type = "JointType.Head"
# random_type = "left_hip"
delimiter = ','


def write_data_to_file(result, save_to_file):
    operation = "a" if os.path.exists(save_to_file) else "w"
    with open(save_to_file, operation) as csvfile:
        csvfile.write(delimiter.join([str(num) for num in result]) + " \n")


def read_features_from_file(filename):
    features = []
    with open(filename) as data:
        lines = data.readlines()
        for i in range(1, len(lines)):
            line = lines[i].split(delimiter)
            for j in range(1, len(line)):
                features.append(float(line[j]))
    return features


def make_header(filepath):
    param_labels = []
    with open(filepath) as data:
        lines = data.readlines()
        params = lines[0].split(delimiter)[1:]
        joint_types = []
        for i in range(1, len(lines)):
            joint_types.append(lines[i].split(delimiter)[0])
        for joint_type in joint_types:
            for param in params:
                param = param.replace("\n", "")
                param_labels.append(joint_type + "-" + param)
    return param_labels


y = []
all_features = []
sample_size = 0
features_count = 0
header = []
codes = ["KT_01",
"KT_02",
"KT_03",
"KT_04",
"KT_05",
"KT_06",
"KT_07",
"KT_08",
"KT_09",
"KT_10",
"KT_11",
"KT_12",
"KT_13",
"KT_15",
"KT_16",
"KT_18",
"KT_19",
"KT_20",
"KT_23",
"KT_24",
"SG01",
"SG02",
"SG03",
"SG04",
"SG05",
"SG07",
"SG08",
"SG09",
"SG11",
"SG13",
"SG14",
"SG15",
"SG16",
"SG17",
"SG18",
"SG19",
"SG21",
"SG22",
"SG23",
"SG24"]

def _get_code_from_dirpath(dirpath):
    head = dirpath
    tail = ''
    while not tail.startswith('KT_') and not tail.startswith('SG'):
        head, tail = os.path.split(head)
    return tail

def collect_data(dir, label):
    global all_features
    global y
    global sample_size
    global features_count
    global header
    for (dirpath, _, filenames) in walk(dir):
        if averages_mm_filename in filenames and averages_angles_filename in filenames and _get_code_from_dirpath(dirpath) in codes:
            sample_size += 1
            mm_filepath = os.path.join(dirpath, averages_mm_filename)
            angles_filepath = os.path.join(dirpath, averages_angles_filename)
            if len(header) == 0:
                header = make_header(mm_filepath) + make_header(angles_filepath)
            print(os.path.join(dirpath, averages_mm_filename))
            features = read_features_from_file(mm_filepath) + read_features_from_file(angles_filepath)
            if features_count == 0:
                features_count = len(features)
            all_features = all_features + features
            y.append(label)


def collect_data_extended(dir, label):
    global all_features
    global y
    global sample_size
    global features_count
    global header
    for (dirpath, _, filenames) in walk(dir):
        # if averages_mm_filename in filenames:
        #     sample_size += 1
        #     mm_filepath = os.path.join(dirpath, averages_mm_filename)
        #     if len(header) == 0:
        #         header = make_header(mm_filepath)
        #     print(os.path.join(dirpath, averages_mm_filename))
        #     features = read_features_from_file(mm_filepath)
        #     if features_count == 0:
        #         features_count = len(features)
        #     all_features = all_features + features
        #     y.append(label)
        if averages_mm_filename2 in filenames:
            sample_size += 1
            mm_filepath = os.path.join(dirpath, averages_mm_filename2)
            if len(header) == 0:
                header = make_header(mm_filepath)
            print(os.path.join(dirpath, averages_mm_filename2))
            features = read_features_from_file(mm_filepath)
            if features_count == 0:
                features_count = len(features)
            all_features = all_features + features
            y.append(label)


def collect_data_diff_weeks(dir, nadal, label):
    global all_features
    global y
    global sample_size
    global features_count
    global header
    for (dirpath, _, filenames) in walk(dir):
        if nadal + averages_mm_part in filenames and nadal + averages_angles_part in filenames:
            sample_size += 1
            mm_filepath = os.path.join(dirpath, nadal + averages_mm_part)
            angles_filepath = os.path.join(dirpath, nadal + averages_angles_part)
            if len(header) == 0:
                header = make_header(mm_filepath) + make_header(angles_filepath)
            print(os.path.join(dirpath, nadal + averages_mm_part))
            features = read_features_from_file(mm_filepath) + read_features_from_file(angles_filepath)
            if features_count == 0:
                features_count = len(features)
            all_features = all_features + features
            y.append(label)


def extract_features_from_files(dirpath, mm_filename, angles_filename, label):
    global sample_size, header, features_count, all_features, y
    sample_size += 1
    mm_filepath = os.path.join(dirpath, mm_filename)
    angles_filepath = os.path.join(dirpath, angles_filename)
    if len(header) == 0:
        header = make_header(mm_filepath) + make_header(angles_filepath)
    print(os.path.join(dirpath, mm_filename))
    features = read_features_from_file(mm_filepath) + read_features_from_file(angles_filepath)
    if features_count == 0:
        features_count = len(features)
    all_features = all_features + features
    y.append(label)


def extract_avg_features_from_files(dirpath, mm_filename, angles_filename, mm_filename2, angles_filename2, label):
    global sample_size, header, features_count, all_features, y
    sample_size += 1
    mm_filepath = os.path.join(dirpath, mm_filename)
    angles_filepath = os.path.join(dirpath, angles_filename)
    mm_filepath2 = os.path.join(dirpath, mm_filename2)
    angles_filepath2 = os.path.join(dirpath, angles_filename2)
    if len(header) == 0:
        header = make_header(mm_filepath) + make_header(angles_filepath)
    print(os.path.join(dirpath, mm_filename))
    features = read_features_from_file(mm_filepath) + read_features_from_file(angles_filepath)
    features2 = read_features_from_file(mm_filepath2) + read_features_from_file(angles_filepath2)
    general_features = []
    for i in range(len(features)):
        general_features.append(average([features[i], features2[i]]))
    if features_count == 0:
        features_count = len(general_features)
    all_features = all_features + general_features
    y.append(label)


def collect_data_by_recordings(dir, nadal, label):
    for (dirpath, _, filenames) in walk(dir):
        mm_filename_i_kord = ""
        angles_filename_i_kord = ""
        mm_filename_ii_kord = ""
        angles_filename_ii_kord = ""
        for filename in filenames:
            if filename.find(nadal) != -1 and filename.find(" I kord") == -1 and filename.find("_mm_avg.csv") != -1:
                mm_filename_i_kord = filename
            elif filename.find(nadal) != -1 and filename.find(" I kord") == -1 and filename.find(
                    "_angles_avg.csv") != -1:
                angles_filename_i_kord = filename
            elif filename.find(nadal) != -1 and filename.find(" II kord") == -1 and filename.find("_mm_avg.csv") != -1:
                mm_filename_ii_kord = filename
            elif filename.find(nadal) != -1 and filename.find(" II kord") == -1 and filename.find(
                    "_angles_avg.csv") != -1:
                angles_filename_ii_kord = filename

        if mm_filename_i_kord != "" and angles_filename_i_kord != "":
            extract_features_from_files(dirpath, mm_filename_i_kord, angles_filename_i_kord, label)
        if mm_filename_ii_kord != "" and angles_filename_ii_kord != "":
            extract_features_from_files(dirpath, mm_filename_ii_kord, angles_filename_ii_kord, label)
        # if mm_filename_i_kord != "" and angles_filename_i_kord != "" and mm_filename_ii_kord != "" and angles_filename_ii_kord != "":
        #     extract_avg_features_from_files(dirpath, mm_filename_i_kord, angles_filename_i_kord, mm_filename_ii_kord,
        #                                     angles_filename_ii_kord, label)


def fisherScore(features, labels):
    f_scores = zeros(len(features.T))
    classes = unique(labels)
    for col in range(len(features.T)):  # calculates Fisher score for each Feature
        c_spread = 0.0  # dispersion of classes (class means relatively to whole feature mean)
        t_spread = 0.0  # whole feature dispersion
        f_mean = mean(features[:, col])  # whole feature mean
        for c in classes:
            nc = len(labels[labels == c]) / len(labels)  # weight of class
            c_mean = mean(features[labels == c, col])  # class mean
            c_std = std(features[labels == c, col])  # class standard deviation
            c_spread += nc * ((c_mean - f_mean) ** 2)
            t_spread += nc * ((c_std) ** 2)
        f_scores[col] += c_spread / t_spread
    return f_scores


c_rootdir = sys.argv[1]
pd_rootdir = sys.argv[2]
save_to_file = sys.argv[3]
f = []

# collect_data_extended(c_rootdir, 0)
# collect_data_extended(pd_rootdir, 1)

collect_data(c_rootdir, 0)
collect_data(pd_rootdir, 1)

# collect_data_diff_weeks(c_rootdir, "nadal_1", 0)
# collect_data_diff_weeks(c_rootdir, "nadal_3", 1)

# collect_data_by_recordings(c_rootdir, " II nadal", 0)
# collect_data_by_recordings(pd_rootdir, " II nadal", 1)

print(sample_size, features_count)

features_array = array(all_features).reshape(sample_size, features_count)
labels_array = array(y).reshape(sample_size, )
fisher_scores = fisher_score(features_array, labels_array)
result = {}
features = ["JointType.KneeRight-eucl_distance",
            "JointType.ShoulderRight-eucl_distance",
            "JointType.HipRight-eucl_distance",
            "JointType.ElbowRight-eucl_distance",

            "JointType.ShoulderRight-velocity",
            "JointType.ShoulderLeft-velocity",
            "JointType.ElbowRight-velocity",
            "JointType.ElbowLeft-velocity",
            "JointType.KneeRight-eucl_distance",
            "JointType.KneeLeft-eucl_distance",

            "JointType.AnkleRight-eucl_distance",
            "JointType.AnkleLeft-eucl_distance",
            "JointType.WristRight-eucl_distance",
            "JointType.WristLeft-eucl_distance",
            "JointType.KneeRight-eucl_distance",
            "JointType.KneeLeft-eucl_distance",
            "JointType.ShoulderRight-eucl_distance",
            "JointType.ShoulderLeft-eucl_distance",
            "JointType.HipRight-eucl_distance",
            "JointType.HipLeft-eucl_distance",
            "JointType.ElbowRight-eucl_distance",
            "JointType.ElbowLeft-eucl_distance"]
features = set(features)
for i in range(len(header)):
    # if header[i] in features:
    result[header[i]] = fisher_scores[i]
result = {"Fisher Score": result}
df = pandas.DataFrame(result).sort_values(by='Fisher Score', ascending=0)
# pandas.DataFrame.to_latex(df, save_to_file)


# print(fisher_scores)
# print(fisherScore(features_array, labels_array))
write_data_to_file(header, save_to_file)
write_data_to_file(fisher_scores, save_to_file)

# write_data_to_file(feature_ranking(fisher_scores), save_to_file)

"""
X samples * features
y samples

C - 0 PD - 1 for I and II
C I - 0 II - 1
PD I - 0 II - 1
PD II - 0 III - 1
PD I - 0 III - 1
by recordings???? combine as if it were twice as much features (or samples) or take average?
"""
