import sys
from os import walk

import numpy as np
import scipy.io

rootdir = sys.argv[1]


def convert_mat_to_csv(dir, mat_file):
    data = scipy.io.loadmat(dirpath + "/" + mat_file)
    for i in data:
        if '__' not in i and 'readme' not in i:
            np.savetxt((dir + "/" + mat_file[:-4] + "_" + i + ".csv"), data[i], delimiter=',')


f = []
for (dirpath, _, filenames) in walk(rootdir):
    for filename in filenames:
        if ".mat" in filename:
            convert_mat_to_csv(dirpath, filename)
