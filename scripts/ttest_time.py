import os
import sys
from collections import OrderedDict
from os import walk

import matplotlib.pyplot as plt
import pandas
import scipy.stats
from numpy import array, zeros, ones, concatenate, column_stack, zeros_like, ones_like, arange

from skfeature.function.similarity_based.fisher_score import fisher_score

experiment_avg = "avg_experiment_time.csv"
walk_avg = "avg_forward_walking_time.csv"
step_avg = "avg_step_time.csv"

delimiter = ','


def write_matrix_to_file(result, filepath):
    data_frame = pandas.DataFrame(result)
    data_frame.to_csv(filepath)


def read_value_from_file(filename):
    with open(filename) as data:
        lines = data.readlines()
        value = float(lines[0])
    return value


def read_matrices_for_subgroup(dir):
    experiment_values = []
    walk_values = []
    step_values = []
    for (dirpath, _, filenames) in walk(dir):
        for filename in filenames:
            if "nadal_2" in dirpath:
                if filename == experiment_avg:
                    ex_val = read_value_from_file(os.path.join(dirpath, filename))
                    if ex_val < 7:
                        print(dirpath)
                    experiment_values.append(ex_val)
                elif filename == walk_avg:
                    w_val = read_value_from_file(os.path.join(dirpath, filename))
                    walk_values.append(w_val)
                elif filename == step_avg:
                    s_val = read_value_from_file(os.path.join(dirpath, filename))
                    step_values.append(s_val)
    print(experiment_values)
    print(walk_values)
    print(step_values)
    print(len(experiment_values), len(walk_values), len(step_values))
    return experiment_values, walk_values, step_values


def make_stat_test(c, pd):
    return scipy.stats.ttest_ind(c, pd, equal_var=False)[1]


def write_matrix_to_file(save_to_file, result):
    data_frame = pandas.DataFrame(result)
    data_frame.to_csv(save_to_file)


c_rootdir = sys.argv[1]
pd_rootdir = sys.argv[2]
save_to_file = sys.argv[3]
f = []

c_ex, c_w, c_s = read_matrices_for_subgroup(c_rootdir)
pd_ex, pd_w, pd_s = read_matrices_for_subgroup(pd_rootdir)


fig = plt.figure(1)
fig.subplots(nrows=3, ncols=1)
plt.subplot(311)
plt.axhline(y=0, color='k')
plt.axhline(y=6, color='k')
plt.scatter(c_ex, zeros_like(c_ex), marker='o', c='blue', zorder=10, alpha=0.7)
plt.scatter(pd_ex, ones_like(pd_ex) * 6, marker='o', c='red', zorder=10, alpha=0.7)
plt.yticks([0, 6], ["C", "PD"])
# plt.xlabel("time (s)")
plt.axis('equal')
plt.subplot(312)
plt.axhline(y=0, color='k')
plt.axhline(y=0.5, color='k')
plt.scatter(c_w, zeros_like(c_w), marker='o', c='blue', zorder=10, alpha=0.7)
plt.scatter(pd_w, ones_like(pd_w) * 0.5, marker='o', c='red', zorder=10, alpha=0.7)
plt.yticks([0, 0.5], ["C", "PD"])
# plt.xlabel("time (s)")
plt.axis('equal')
plt.subplot(313)
plt.axhline(y=-0.023, color='k')
plt.axhline(y=0.023, color='k')
plt.scatter(c_s, ones_like(c_s) * -0.023, marker='o', c='blue', zorder=10, alpha=0.7)
plt.scatter(pd_s, ones_like(pd_s) * 0.023, marker='o', c='red', zorder=10, alpha=0.7)
plt.axis('equal')
plt.yticks([-0.023, 0.023], ["C", "PD"])
# plt.xlabel("time (s)")
# plt.savefig("/home/anna/Documents/Masters/plots/time_v2/all.pdf", bbox_inches='tight')
plt.clf()


max_x = max(c_w + pd_w)
fig = plt.figure(figsize=(max_x, 1))
plt.axhline(y=0, color='k')
plt.axhline(y=6, color='k')
plt.scatter(c_ex, zeros_like(c_ex), marker='o', c='blue', zorder=10, alpha=0.7)
plt.scatter(pd_ex, ones_like(pd_ex) * 6, marker='o', c='red', zorder=10, alpha=0.7)
plt.xlabel("time (s)")
plt.axis('equal')
plt.yticks([0, 6], ["C", "PD"])
# plt.savefig("/home/anna/Documents/Masters/plots/time_v2/experiment.pdf", bbox_inches='tight')
plt.clf()

# fig = plt.figure(figsize=(max_x, 1))
# plt.axhline(y=0, color='k')
# plt.axhline(y=0.5, color='k')
# plt.scatter(c_w, zeros_like(c_w), marker='o', c='blue', zorder=10, alpha=0.7)
# plt.scatter(pd_w, ones_like(pd_w) * 0.5, marker='o', c='red', zorder=10, alpha=0.7)
# plt.xlabel("time (s)")
# plt.axis('equal')
# plt.yticks([0, 0.5], ["C", "PD"])
# plt.savefig("/home/anna/Documents/Masters/plots/time_v2/walking_forward.pdf", bbox_inches='tight')
# plt.clf()
#
# fig = plt.figure(figsize=(max_x, 1))
# plt.axhline(y=-0.023, color='k')
# plt.axhline(y=0.023, color='k')
# plt.scatter(c_s, ones_like(c_s) * -0.023, marker='o', c='blue', zorder=10, alpha=0.7)
# plt.scatter(pd_s, ones_like(pd_s) * 0.023, marker='o', c='red', zorder=10, alpha=0.7)
# plt.xlabel("time (s)")
# plt.axis('equal')
# plt.yticks([-0.023, 0.023], ["C", "PD"])
# plt.savefig("/home/anna/Documents/Masters/plots/time_v2/step.pdf", bbox_inches='tight')
# plt.clf()

ttest_results = [make_stat_test(c_ex, pd_ex), make_stat_test(c_w, pd_w), make_stat_test(c_s, pd_s)]
# print(ttest_results)

sample_size = len(c_ex) + len(pd_ex)
features_count = 3
ex = concatenate((c_ex, pd_ex))
w = concatenate((c_w, pd_w))
s = concatenate((c_s, pd_s))
all_features = column_stack((ex.T, w.T, s.T))
# print(all_features)
y = concatenate((zeros((len(c_ex),)), ones((len(pd_ex),))))

features_array = array(all_features).reshape(sample_size, features_count)
labels_array = array(y).reshape(sample_size, )
# print(features_array)
# print(labels_array)

fisher_scores = fisher_score(features_array, labels_array)
print(fisher_scores)

ttest_dict = OrderedDict([("Overall Time", ttest_results[0]),
                          ("Walking Forward Time", ttest_results[1]),
                          ("Step Time", ttest_results[2])])
fisher_dict = OrderedDict([("Overall Time", fisher_scores[0]),
                           ("Walking Forward Time", fisher_scores[1]),
                           ("Step Time", fisher_scores[2])])
data = OrderedDict([("p-value", ttest_dict),
                    ("Fisher score", fisher_dict)])
data_frame = pandas.DataFrame(data)
data_frame = data_frame.sort_values(by="p-value")
# print(data_frame)
# data_frame.to_latex("/home/anna/Documents/Masters/plots/time_v2/time.tex")

# write_matrix_to_file(data, "/home/anna/Documents/Masters/tables/time.csv")
