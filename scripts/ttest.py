import math
import os
import pandas
import scipy.stats
import sys
from os import walk
import tabulate

averages_filename = "nadal_2_avg_Kondimised_mm_steps_week.csv"
averages_filename2 = "nadal_2_avg_Alustamised_mm_steps_week.csv"
# general_averages = "average angles.csv"
random_type = "JointType.Head"
# random_type = "left_hip"
delimiter = ','
ignored = [ "eucl_div_leg_length", "acceleration", "jerk", "eucl_div_acceleration", "eucl_div_trajectory", "time"]


def read_matrix_from_file(filename):
    matrix = {}
    with open(filename) as data:
        lines = data.readlines()
        params = lines[0].split(delimiter)
        for i in range(len(params)):
            params[i] = params[i].replace("\n", "")
        for i in range(1, len(lines)):
            matrix_line = {}
            line = lines[i].split(delimiter)
            for j in range(1, len(line)):
                param = params[j]
                matrix_line[param] = float(line[j])
                if math.isnan(float(line[j])):
                    print(filename)
            joint_type = lines[i].split(delimiter)[0]
            matrix[joint_type] = matrix_line
    return matrix


def read_matrices_for_subgroup(dir):
    matrices = []
    for (dirpath, _, filenames) in walk(dir):
        if averages_filename in filenames:
            print(os.path.join(dirpath, averages_filename))
            matrices.append(read_matrix_from_file(os.path.join(dirpath, averages_filename)))
        if averages_filename2 in filenames:
            print(os.path.join(dirpath, averages_filename2))
            matrices.append(read_matrix_from_file(os.path.join(dirpath, averages_filename2)))
    return matrices


def read_matrices_by_filename(dir, filename):
    matrices = []
    for (dirpath, _, filenames) in walk(dir):
        if filename in filenames:
            print(os.path.join(dirpath, filename))
            matrices.append(read_matrix_from_file(os.path.join(dirpath, filename)))
    return matrices


def read_matrices_by_part_of_filename(dir, part_of_filename, part2):
    matrices = []
    for (dirpath, _, filenames) in walk(dir):
        for filename in filenames:
            if part_of_filename in filename and part2 in filename:
                print(os.path.join(dirpath, filename))
                matrices.append(read_matrix_from_file(os.path.join(dirpath, filename)))
    return matrices


def read_parameters_from_matrices(matrices):
    result_matrix = {}
    for joint_type, params in matrices[0].items():
        matrix_line = {}
        for param, value in params.items():
            # if param in ignored:
            #     continue
            values = []
            for matrix in matrices:
                values.append(matrix[joint_type][param])
            matrix_line[param] = values
        result_matrix[joint_type] = matrix_line
    return result_matrix


def make_stat_test(c, pd):
    result = 0
    if scipy.stats.ttest_ind(c, pd, equal_var=False)[1] <= 0.05:
        result = 1
        # result = scipy.stats.ttest_ind(c, pd, equal_var=False)[1]
    return result
    # return scipy.stats.ttest_ind(c, pd, equal_var=False)[1]


def write_matrix_to_file(save_to_file, result):
    data_frame = pandas.DataFrame(result)
    data_frame.to_csv(save_to_file)


c_rootdir = sys.argv[1]
pd_rootdir = sys.argv[2]
save_to_file = sys.argv[3]
f = []


c_matrices = read_matrices_for_subgroup(c_rootdir)
pd_matrices = read_matrices_for_subgroup(pd_rootdir)

# c_matrices = read_matrices_by_filename(c_rootdir, "nadal_1_avg_Kondimised_angles_steps_week.csv")
# pd_matrices = read_matrices_by_filename(pd_rootdir, "nadal_3_avg_Kondimised_angles_steps_week.csv")

# c_matrices = read_matrices_by_part_of_filename(c_rootdir, " II nadal", "_mm_avg")
# pd_matrices = read_matrices_by_part_of_filename(pd_rootdir, " II nadal", "_mm_avg")

c_matrix = read_parameters_from_matrices(c_matrices)
pd_matrix = read_parameters_from_matrices(pd_matrices)

result = {}

if len(c_matrix) == len(pd_matrix) and len(c_matrix[random_type]) == len(pd_matrix[random_type]):
    for joint_type, params in c_matrix.items():
        result_line = {}
        for param in params:
            stat_result = make_stat_test(c_matrix[joint_type][param], pd_matrix[joint_type][param])
            result_line[param] = stat_result
        result[joint_type] = result_line
    # input('Press enter to continue: ')
    write_matrix_to_file(save_to_file, result)
    # print(tabulate.tabulate(result, tablefmt='latex'))
    # df = pandas.DataFrame(result).transpose()
    # pandas.DataFrame.to_latex(df, save_to_file)
