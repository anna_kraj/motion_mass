import numpy
import os
import pandas
import sys
from os import walk

general_averages = "averages.csv"
delimiter = ','


def calc_avgs(matrix):
    params = {}
    for i in range(1, len(matrix[0])):
        param = matrix[0][i]
        params[param] = {}
        for j in range(1, len(matrix)):
            line = matrix[j]
            joint_type = line[0]
            if joint_type not in params[param].keys():
                params[param][joint_type] = []
            try:
                params[param][joint_type].append(float(matrix[j][i]))
            except:
                print("error")
    for param, values in params.items():
        for joint_type, values_list in values.items():
            params[param][joint_type] = numpy.average(values_list)
    return params


def number_of_tokens_in_first_line(file_string):
    return len(file_string[0].split(delimiter))


def read_matrix_from_file(filename):
    matrix = []
    with open(filename) as data:
        for line in data:
            matrix_line = []
            for value in line.split(delimiter):
                matrix_line.append(value.replace('\n', ''))
            matrix.append(matrix_line)
    return matrix


def write_matrix_to_file(result, save_to_file):
    data_frame = pandas.DataFrame(result)
    data_frame.to_csv(save_to_file)


rootdir = sys.argv[1]
f = []

for (dirpath, _, filenames) in walk(rootdir):
    if '_avg_Kondimised_' not in filenames:
        matrices = []
        for filename in filenames:
            if '_steps_week' in filename:
                head, tail = os.path.split(dirpath)
                week_avg_filename = os.path.join(dirpath, tail + '_avg_' + filename)
                matrix = read_matrix_from_file(os.path.join(dirpath, filename))
                result_matrix = calc_avgs(matrix)
                print(dirpath + '/' + filename)
                # input('Press enter to continue: ')
                write_matrix_to_file(result_matrix, os.path.join(dirpath, week_avg_filename))
