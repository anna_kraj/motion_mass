import sys
from os import walk
import os
import pandas
import numpy

general_averages = "averages.csv"
delimiter = ','


def calc_avgs(matrices):
    num_of_matrices = len(matrices)
    matrix_size = len(matrices[0])
    matrix_line_size = len(matrices[0][0])
    result_matrix = matrices.pop(0)
    for i in range(1, matrix_size):
        for j in range(1, matrix_line_size):
            result_matrix[i][j] = float(result_matrix[i][j])
    for matrix in matrices:
        if len(matrix) != matrix_size:
            return []
        for i in range(1, matrix_size):
            for j in range(1, matrix_line_size):
                result_matrix[i][j] += float(matrix[i][j])
    for i in range(1, matrix_size):
        for j in range(1, matrix_line_size):
            result_matrix[i][j] /= num_of_matrices
    return result_matrix


def number_of_tokens_in_first_line(file_string):
    return len(file_string[0].split(delimiter))


def read_matrix_from_file(filename):
    matrix = []
    with open(filename) as data:
        for line in data:
            matrix_line = []
            for value in line.split(delimiter):
                matrix_line.append(value)
            matrix.append(matrix_line)
    return matrix


# def write_matrix_to_file(result_matrix, dirpath):
#     data = numpy.asarray(result_matrix)
#     file_path = dirpath + "/" + general_averages
#     numpy.savetxt(file_path, data, fmt="%s", delimiter=delimiter)
#     return

def write_matrix_to_file(result, save_to_file):
    data_frame = pandas.DataFrame(result)
    data_frame.to_csv(save_to_file)


rootdir = sys.argv[1]
f = []

for (dirpath, _, filenames) in walk(rootdir):
    if general_averages not in filenames:
        matrices = []
        for filename in filenames:
            if '_avg_walk_' in filename:
                matrices.append(read_matrix_from_file(os.path.join(dirpath, filename)))
        if len(matrices) > 0:
            result_matrix = calc_avgs(matrices)
            write_matrix_to_file(result_matrix, os.path.join(dirpath, general_averages))
