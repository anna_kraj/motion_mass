import sys

import numpy as np
import pandas
from scipy.stats import ttest_ind


def make_stat_test(c, pd):
    result = 0
    if ttest_ind(c, pd, equal_var=False)[1] <= 0.05:
        result = 1
        # result = scipy.stats.ttest_ind(c, pd, equal_var=False)[1]
    return result
    # return scipy.stats.ttest_ind(c, pd, equal_var=False)[1]


def perform_ttest_on_data_from_two_files(filename1, filename2, save_to_file=None):
    df1 = pandas.read_csv(filename1, index_col=0)
    df2 = pandas.read_csv(filename2, index_col=0)
    intersection = df1.index.intersection(df2.index)
    c = [code for code in list(intersection) if code.startswith('KT')]
    pd = [code for code in list(intersection) if code.startswith('SG')]
    c1 = df1.loc[c]
    c2 = df2.loc[c]
    p_val = ttest_ind(c1, c2, equal_var=True)[1]
    print(p_val <= 0.05)
    for key, p_val in zip(c1.columns.values, p_val):
        if p_val <= 0.05:
            print(key, '\t', p_val)
    pd1 = df1.loc[pd]
    pd2 = df2.loc[pd]

    p_val = ttest_ind(pd1, pd2, equal_var=True)[1]
    print(p_val <= 0.05)
    for key, p_val in zip(pd1.columns.values, p_val):
        if p_val <= 0.05:
            print(key, '\t', p_val)
    if save_to_file is not None:
        pandas.DataFrame.to_latex(df, save_to_file, column_format='')


def perform_ttest_on_data_from_one_file(filename, save_to_file=None):
    df = pandas.read_csv(filename, index_col=0)
    c = [code for code in list(df.index) if code.startswith('KT')]
    pd = [code for code in list(df.index) if code.startswith('SG')]
    c_data = df.loc[c]
    pd_data = df.loc[pd]
    p_val = ttest_ind(c_data, pd_data, equal_var=True)[1]
    index = [feature_name_for_tables(feature) for feature in c_data.columns.values]
    p_val_series = pandas.DataFrame(p_val, index=index, columns=['p value'])
    p_val_series.sort_values('p value', inplace=True)
    p_val_series['p <= 0.1'] = np.where(p_val_series['p value'] <= 0.1, 1, 0)
    print(p_val_series)
    if save_to_file is not None:
        pandas.DataFrame.to_latex(p_val_series, save_to_file, column_format='|l|c|c|c|')
    # print(p_val <= 0.05)
    # for key, p_val in zip(c_data.columns.values, p_val):
    #     if p_val <= 0.05:
    #         print(key, '\t', p_val)


def shorten_feature_names(features, shorten_sides=True):
    shortened = []
    for feature in features:
        short = feature.replace('JointType.', '') \
            .replace('-eucl_distance', ' E') \
            .replace('-velocity', ' Vm') \
            .replace('-trajectory', ' Tm') \
            .replace('-eucl_div_acceleration', ' E/Am') \
            .replace('-eucl_div_trajectory', ' E/Tm') \
            .replace('-acceleration', ' Am') \
            .replace('-jerk', ' Jm') \
            .replace('-time', ' t')
        if shorten_sides:
            short = short.replace('Right', ' R') \
                .replace('Left', ' L')
        shortened.append(short)
    return shortened


def feature_name_for_tables(feature):
    shortened = shorten_feature_names([feature], shorten_sides=False)[0]
    shortened = shortened.split()
    if len(shortened) > 1:
        return " & ".join(shortened)
    else:
        return shortened[0] + " & "


if __name__ == '__main__':
    perform_ttest_on_data_from_one_file(
        sys.argv[1])
