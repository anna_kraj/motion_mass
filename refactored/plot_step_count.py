from numpy import zeros_like, ones_like
import matplotlib.pyplot as plt
from pandas import read_csv

filename = '/home/anna/Documents/Masters/Masters/results3/kondimised/features/2/all_times.csv'
df = read_csv(filename, index_col=0)
c = [code for code in list(df.index) if code.startswith('KT')]
pd = [code for code in list(df.index) if code.startswith('SG')]
c_data = df.loc[c]
pd_data = df.loc[pd]

c_ex, c_w, c_s = c_data['test'], c_data['walk forward'], c_data['step']
pd_ex, pd_w, pd_s = pd_data['test'], pd_data['walk forward'], pd_data['step']

fig = plt.figure(1)
fig.subplots(nrows=3, ncols=1)
# fig.subplots_adjust(wspace=0.5)
plt.subplot(311)
plt.title('Overall Time')
plt.axhline(y=0, color='k')
plt.axhline(y=6, color='k')
plt.scatter(c_ex, zeros_like(c_ex), marker='o', c='blue', zorder=10, alpha=0.7)
plt.scatter(pd_ex, ones_like(pd_ex) * 6, marker='o', c='red', zorder=10, alpha=0.7)
plt.yticks([0, 6], ["C", "PD"])
# plt.xlabel("time (s)")
# plt.savefig("/home/anna/Documents/Masters/plots/time_v2/all.pdf", bbox_inches='tight')
# plt.clf()
