import numpy as np
from scipy.signal import argrelextrema

import draw
import kinect
import parameters
from kinect import Body

PHASE_MAP = {'stand_up': 62,
             'walk_forward': 63,
             'turn_around': 64,
             'walk_back': 65,
             'turn_back_around': 66,
             'sit_down': 67}

# PHASE_MAP = {'walk_forward': 62,
#              'turn_around': 63,
#              'walk_back': 64,
#              'turn_back_around': 65}


def get_parameters_by_steps(recording):
    mm_params = []
    angle_params = []
    for experiment in recording.experiments:
        walking_forward_phase = experiment.phases['walk_forward']
        mm_params += walking_forward_phase.mm_params
        angle_params += walking_forward_phase.angle_params
    return mm_params, angle_params


def get_parameters(recording, phase_name):
    mm_params = []
    angle_params = []
    for experiment in recording.experiments:
        phase = experiment.phases[phase_name]
        mm_params += phase.mm_params_sum
        angle_params += phase.angle_params_sum
    return mm_params, angle_params


def get_experiment_times_sum(recording):
    times = []
    experiments = []
    if len(recording.experiments) >= 3:
        experiments = recording.experiments[:3]
    for experiment in experiments:
        times.append(experiment.t)
    return sum(times)


def get_experiment_num_of_steps(recording):
    num_of_steps = 0
    if len(recording.experiments) >= 3:
        experiments = recording.experiments[:3]
        for experiment in experiments:
            phase = experiment.phases['walk_forward']
            num_of_steps += len(phase.steps)
    return num_of_steps


def get_steps(recording):
    steps = []
    for experiment in recording.experiments:
        steps += experiment.phases['walk_forward'].df_steps_list
    return steps


class Recording(object):

    def __init__(self, df):
        self.experiments = self.extract_experiments(df)

    @staticmethod
    def extract_experiments(df):
        no_phase = df
        for phase in PHASE_MAP.values():
            no_phase = no_phase[no_phase.iloc[:, phase] == 0]
        indices = list(no_phase.index)
        ranges = []
        if indices[0] != df.index[0]:
            ranges.append(df.iloc[0:indices[0]])
        diff = indices[0]
        for i, idx in enumerate(indices):
            if idx - i != diff:
                range_start = indices[i - 1] + 1
                range_end = idx
                ranges.append(df.iloc[range_start:range_end])
                diff = idx - i
        experiments = []
        for r in ranges:
            experiments.append(Experiment(r))
        return experiments


class Experiment(object):

    def __init__(self, df):
        self.phases = {}
        self.t = kinect.get_seconds(df.iloc[-1, 1] - df.iloc[0, 1])
        for phase_name in PHASE_MAP.keys():
            print(phase_name)
            self.phases[phase_name] = self.extract_phase(df, phase_name)

    @staticmethod
    def extract_phase(df, phase):
        column_nr = PHASE_MAP[phase]
        df.iloc[:, column_nr].astype('int', copy=False)
        if phase == 'walk_forward':
            return WalkingForwardPhase(df.loc[df.iloc[:, column_nr] == 1].iloc[:, 0:PHASE_MAP['stand_up']])
        return Phase(df.loc[df.iloc[:, column_nr] == 1].iloc[:, 0:PHASE_MAP['stand_up']])
        # if phase == 'walk_forward':
        #     return WalkingForwardPhase(df.loc[df.iloc[:, column_nr] == 1].iloc[:, 0:PHASE_MAP['walk_forward']])
        # return Phase(df.loc[df.iloc[:, column_nr] == 1].iloc[:, 0:PHASE_MAP['walk_forward']])


class Phase(object):

    def __init__(self, df):
        self.skeletons = self.extract_skeletons(df)
        self.mm_params_sum = []
        self.angle_params_sum = []
        self.mm_params_sum.append(parameters.calculate_parameters(self.skeletons))
        self.angle_params_sum.append(parameters.calculate_angles(self.skeletons))

    @staticmethod
    def extract_skeletons(df):
        skeletons = []
        for i in range(len(df.index)):
            row = df.iloc[i]
            skeletons.append(Body.from_df_row(row))
        return skeletons


class WalkingForwardPhase(Phase):

    def __init__(self, df):
        Phase.__init__(self, df)
        self.df = df
        self.steps, self.df_steps_list = self.extract_steps()
        self.mm_params = []
        self.angle_params = []
        for step in self.steps:
            self.mm_params.append(parameters.calculate_parameters(step))
            self.angle_params.append(parameters.calculate_angles(step))

    def extract_steps(self):  # TODO add first step recognition, not minima, but from minimum value
        step_indices = []
        distances = self.calculate_distances_between_ankles()
        smoothed = [0] + parameters.smooth_using_moving_average(3, distances)
        abs_smoothed = []
        for dist in smoothed:
            abs_smoothed.append(abs(dist))
        indices = argrelextrema(np.array(abs_smoothed), np.less)[0]
        for i in range(len(indices)):
            index = indices[i]
            if i + 1 == len(indices):
                last = len(abs_smoothed) - 1
            else:
                last = indices[i + 1]
            if abs_smoothed[index] < 0.07 and (max(abs_smoothed[index:last]) > 0.12 or (
                    i == len(indices) - 1 and max(abs_smoothed[max(0, i - 1):index]) > 0.12)):
                step_indices.append(index)
        steps = []
        df_step_list = []
        if len(step_indices) > 1:
            for i in range(1, len(step_indices)):
                steps.append(self.skeletons[step_indices[i - 1]: step_indices[i] + 1])
                df_step_list.append(self.df.iloc[step_indices[i - 1]:step_indices[i]])
        # draw.draw_steps(distances, abs_smoothed, step_indices)
        return steps, df_step_list

    def calculate_distances_between_ankles(self):
        distances = []
        for i in range(len(self.skeletons)):
            body = self.skeletons[i]
            distances.append(body.calc_distance_between_ankles())
        return distances
