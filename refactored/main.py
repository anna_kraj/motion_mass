import os
import sys
from collections import defaultdict
from os import walk

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from draw import draw_body
from refactored.experiment import Recording, get_parameters_by_steps, get_experiment_times_sum, get_parameters, get_steps, get_experiment_num_of_steps


def write_df_to_file(df, save_to_file):
    if not os.path.exists(save_to_file):
        df.to_csv(save_to_file)


def write_or_append_to_csv(df, save_to_file):
    if not os.path.exists(save_to_file):
        df.to_csv(save_to_file)
    else:
        with open(save_to_file, 'a') as f:
            df.to_csv(f, header=False)


def write_data_to_file(data, save_to_file):
    with open(save_to_file, 'w') as f:
        f.write(data)


def convert_list_of_dict_to_dict_of_avg(list_of_dict):
    result = defaultdict(dict)
    for d in list_of_dict:
        for param, joints_dict in d.items():
            if param not in result.keys():
                result[param] = defaultdict(list)
            for joint_type, value in joints_dict.items():
                try:
                    short = str(joint_type).split('.')[1]
                except IndexError:
                    short = joint_type
                result[param][short].append(value)
    for param, joints_dict in result.items():
        for joint_type, values in joints_dict.items():
            result[param][joint_type] = np.average(values)
    return pd.DataFrame.from_dict(result)


def extract_step_info(df_list, code, week, folder_to_write_to=None):
    mm_params_by_step = []
    angle_params_by_step = []
    times = []
    step_amounts = []
    for df in df_list:
        recording = Recording(df)
        times += [get_experiment_times_sum(recording)]
        step_amounts += [get_experiment_num_of_steps(recording)]
        mm, angles = get_parameters_by_steps(recording)
        mm_params_by_step += mm
        angle_params_by_step += angles
    avg_mm_params_df = convert_list_of_dict_to_dict_of_avg(mm_params_by_step)
    avg_angle_params_df = convert_list_of_dict_to_dict_of_avg(angle_params_by_step)
    avg_time = np.average(times)
    avg_steps = np.average(step_amounts)
    if folder_to_write_to is not None:
        folder_to_write_to = os.path.join(folder_to_write_to, code, week)
        os.makedirs(folder_to_write_to, exist_ok=True)
        # average_file_mm = code + '_' + week + '_mm.csv'
        # average_file_angles = code + '_' + week + '_angles.csv'
        # average_file_t = code + '_' + week + '_t.csv'
        # write_df_to_file(avg_mm_params_df, os.path.join(folder_to_write_to, average_file_mm))
        # write_df_to_file(avg_angle_params_df, os.path.join(folder_to_write_to, average_file_angles))
        # write_data_to_file(str(avg_time), os.path.join(folder_to_write_to, average_file_t))
        write_data_to_file(str(avg_steps), os.path.join(folder_to_write_to, f'{code}_{week}_step_count.csv'))


def extract_phase_info(df_list, code, week, folder_to_write_to=None):
    mm_walk_forward = []
    mm_stand_up = []
    mm_sit_down = []
    mm_turn_around = []
    mm_turn_back_around = []
    angles_walk_forward = []
    angles_stand_up = []
    angles_sit_down = []
    angles_turn_around = []
    angles_turn_back_around = []
    for df in df_list:
        recording = Recording(df)

        mm, angles = get_parameters(recording, 'walk_forward')
        mm_walk_forward += mm
        angles_walk_forward += angles

        mm, angles = get_parameters(recording, 'stand_up')
        mm_stand_up += mm
        angles_stand_up += angles

        mm, angles = get_parameters(recording, 'sit_down')
        mm_sit_down += mm
        angles_sit_down += angles

        mm_turn_around += mm
        mm, angles = get_parameters(recording, 'turn_around')
        angles_turn_around += angles

        mm, angles = get_parameters(recording, 'turn_back_around')
        mm_turn_back_around += mm
        angles_turn_back_around += angles

    avg_mm_walk_forward = convert_list_of_dict_to_dict_of_avg(mm_walk_forward)
    avg_mm_stand_up = convert_list_of_dict_to_dict_of_avg(mm_stand_up)
    avg_mm_sit_down = convert_list_of_dict_to_dict_of_avg(mm_sit_down)
    avg_mm_turn_around = convert_list_of_dict_to_dict_of_avg(mm_turn_around)
    avg_mm_turn_back_around = convert_list_of_dict_to_dict_of_avg(mm_turn_back_around)

    avg_angles_walk_forward = convert_list_of_dict_to_dict_of_avg(angles_walk_forward)
    avg_angles_stand_up = convert_list_of_dict_to_dict_of_avg(angles_stand_up)
    avg_angles_sit_down = convert_list_of_dict_to_dict_of_avg(angles_sit_down)
    avg_angles_turn_around = convert_list_of_dict_to_dict_of_avg(angles_turn_back_around)
    avg_angles_turn_back_around = convert_list_of_dict_to_dict_of_avg(angles_turn_back_around)
    if folder_to_write_to is not None:
        folder_to_write_to = os.path.join(folder_to_write_to, code, week, 'phases')
        os.makedirs(folder_to_write_to, exist_ok=True)
        average_file_walk_forward = code + '_' + week + '_walk_forward_mm.csv'
        average_file_stand_up = code + '_' + week + '_stand_up_mm.csv'
        average_file_sit_down = code + '_' + week + '_sit_down_mm.csv'
        average_file_turn_around = code + '_' + week + '_turn_around_mm.csv'
        average_file_turn_back_around = code + '_' + week + '_turn_back_around_mm.csv'
        write_df_to_file(avg_mm_walk_forward, os.path.join(folder_to_write_to, average_file_walk_forward))
        write_df_to_file(avg_mm_stand_up, os.path.join(folder_to_write_to, average_file_stand_up))
        write_df_to_file(avg_mm_sit_down, os.path.join(folder_to_write_to, average_file_sit_down))
        write_df_to_file(avg_mm_turn_around, os.path.join(folder_to_write_to, average_file_turn_around))
        write_df_to_file(avg_mm_turn_back_around, os.path.join(folder_to_write_to, average_file_turn_back_around))

        average_file_walk_forward = code + '_' + week + '_walk_forward_angles.csv'
        average_file_stand_up = code + '_' + week + '_stand_up_angles.csv'
        average_file_sit_down = code + '_' + week + '_sit_down_angles.csv'
        average_file_turn_around = code + '_' + week + '_turn_around_angles.csv'
        average_file_turn_back_around = code + '_' + week + '_turn_back_around_angles.csv'
        write_df_to_file(avg_angles_walk_forward, os.path.join(folder_to_write_to, average_file_walk_forward))
        write_df_to_file(avg_angles_stand_up, os.path.join(folder_to_write_to, average_file_stand_up))
        write_df_to_file(avg_angles_sit_down, os.path.join(folder_to_write_to, average_file_sit_down))
        write_df_to_file(avg_angles_turn_around, os.path.join(folder_to_write_to, average_file_turn_around))
        write_df_to_file(avg_angles_turn_back_around, os.path.join(folder_to_write_to, average_file_turn_back_around))


def extract_steps(df_list, data_filenames, code, week, folder_to_write_to=None):
    for df, filename in zip(df_list, data_filenames):
        _, filename_tail = os.path.split(filename)
        recording = Recording(df)
        steps = get_steps(recording)
        if folder_to_write_to is not None:
            folder_to_write_to = os.path.join(folder_to_write_to, code, week)
            os.makedirs(folder_to_write_to, exist_ok=True)
            filename = code + '_' + week + '_' + filename_tail.split('.')[0] + '_steps.csv'
            for i, step in enumerate(steps):
                step['step_id'] = i
                print(step)
                write_or_append_to_csv(step,
                                       os.path.join(folder_to_write_to, filename))


def draw_phases(filenames):
    code, week = extract_code_and_week_from_dirpath(filenames[0])
    for filename in filenames:
        df = pd.read_csv(filename, delimiter=',', header=None)
        recording = Recording(df)
        for k, phase in recording.experiments[0].phases.items():
            for body in phase.skeletons:
                plt.clf()
                draw_body(body)
                # plt.show()
                plt.pause(0.08)
            plt.pause(2)


def extract_dfs(filenames, num_of_phases=6):
    used_filenames = []
    df_list = []
    for filename in filenames:
        df = pd.read_csv(filename, delimiter=',', header=None)
        print(len(df.columns[62:]))
        print(df.iloc[:, 62:].sum()[6:].eq(0).all())
        if len(df.columns[62:]) == num_of_phases:
            df_list.append(df)
            used_filenames.append(filename)
        elif df.iloc[:, 62:].sum()[num_of_phases:].eq(0).all():
            df_list.append(df)
            used_filenames.append(filename)
    return df_list, used_filenames


def extract_code_and_week_from_dirpath(dirpath):
    head, tail = os.path.split(dirpath)
    code = ''
    week = ''
    while head != '/' and head != '':
        head, tail = os.path.split(head)
        if 'SG' in tail or 'KT' in tail:
            code = tail
        elif 'nadal' in tail:
            week = tail.split('_')[1]
    return code, week


if __name__ == '__main__':
    rootdir = sys.argv[1]
    for (dirpath, _, filenames) in walk(rootdir):
        if 'Kondimised' in dirpath:
            data_filenames = [os.path.join(dirpath, f) for f in filenames if 'kord_data.csv' in f]
            print(data_filenames)
            df_list, data_filenames = extract_dfs(data_filenames, 6)
            code, week = extract_code_and_week_from_dirpath(dirpath)
            if len(df_list) > 0:
                print(data_filenames)
                # extract_steps(df_list, data_filenames, code, week, '/home/anna/Desktop/tiina_sammud')
                extract_step_info(df_list, code, week, '/home/anna/Documents/Masters/Masters/results4/kondimised')
                # extract_phase_info(df_list, code, week, '/home/anna/Documents/Masters/Masters/results3/kondimised')
        # if 'Alustamised' in dirpath or 'AlustamisedU' in dirpath:
        #     data_filenames = [os.path.join(dirpath, f) for f in filenames if '_data.csv' in f]
        #     df_list, data_filenames = extract_dfs(data_filenames, 4)
        #     code, week = extract_code_and_week_from_dirpath(dirpath)
        #     if len(df_list) > 0:
        #         print(data_filenames)
        #         extract_step_info(df_list, code, week, '/home/anna/Documents/Masters/Masters/results2/alustamised')
        #         extract_phase_info(df_list, code, week, '/home/anna/Documents/Masters/Masters/results2/alustamised')
