import os
import sys

import pandas

from experiment import separate_walks, calculate_experiment_durations
from kinect import KinectVersion, JointType
from reader import CSVReader


def write_data_to_file(result, save_to_file):
    data_frame = pandas.DataFrame(result)
    if not os.path.exists(save_to_file):
        data_frame.to_csv(save_to_file, header=False, index=False)
    else:
        with open(save_to_file, 'a') as csvfile:
            data_frame.to_csv(csvfile, header=False, index=False)


def main(dirpath_to_read_from, dirpath_to_write_to, filename, column):
    filepath = os.path.join(dirpath_to_read_from, filename)
    data = CSVReader(filepath).read()
    # print(len(data[-1]))
    walks = separate_walks(data, column, KinectVersion.FIRST)
    experiments = calculate_experiment_durations(data, KinectVersion.FIRST)
    experiment_t = []
    experiment_t2 = []
    experiment_t3 = []
    if len(experiments) != 3:
        print("error\t", len(experiments), "\t", filepath)
        return
    else:
        experiment_t = experiment_t + experiments
        experiment_t2.append(sum(experiments[0:2]))
        experiment_t3.append(sum(experiments))
    walk_t = []
    step_t = []
    for walk in walks:
        if len(walk.bodies) > 0:
            walk_t1 = walk.bodies[0].joints[JointType.Head].timestamp
            walk_t2 = walk.bodies[-1].joints[JointType.Head].timestamp
            walk_t.append(abs(walk_t2 - walk_t1))
            steps = walk.detect_steps()
            for step in steps:
                step_t1 = step[0].joints[JointType.Head].timestamp
                step_t2 = step[-1].joints[JointType.Head].timestamp
                step_t.append(abs(step_t2 - step_t1))
    # print("experiment", experiment_t)
    # print("walk", walk_t)
    # print("steps", step_t)

    if len(experiment_t) > 0 and len(walk_t) > 0 and len(step_t) > 0:
    # if len(walk_t) > 0 and len(step_t) > 0:
        write_data_to_file(experiment_t, os.path.join(dirpath_to_write_to, "experiment_time.csv"))
        write_data_to_file(experiment_t2, os.path.join(dirpath_to_write_to, "experiment_time2.csv"))
        write_data_to_file(experiment_t3, os.path.join(dirpath_to_write_to, "experiment_time3.csv"))
        write_data_to_file(walk_t, os.path.join(dirpath_to_write_to, "forward_walking_time.csv"))
        write_data_to_file(step_t, os.path.join(dirpath_to_write_to, "step_time.csv"))


if __name__ == '__main__':
    rootdir = sys.argv[1]
    column = int(sys.argv[2])
    for (dirpath, _, filenames) in os.walk(rootdir):
        for filename in filenames:
            if ".csv" in filename:
                if 'Kondimised' in dirpath.split('/') and '_data.csv' in filename:
                    print(dirpath + '/' + filename)
                    head, tail = os.path.split(dirpath)
                    # head, tail = os.path.split(head)
                    # input('Press enter to continue: ')
                    main(dirpath, head, filename, column)
