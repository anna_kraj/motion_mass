import numpy as np
from scipy.signal import argrelextrema

import kinect
import parameters


def separate_walks(data, column, version=kinect.KinectVersion.SECOND):
    walks = []
    walk = Walk()
    i = 0
    for row in data:
        i = i + 1
        if version == kinect.KinectVersion.FIRST:
            if float(row[column]) == 1:
                walk.add_frame(row, version)
            else:
                if len(walk.bodies) > 0:
                    walks.append(walk)
                    walk = Walk()
        else:
            walk.add_frame(row, version)
    if len(walks) == 0:
        walks.append(walk)
    return walks


def calculate_experiment_durations(data, version=kinect.KinectVersion.SECOND):
    durations = []
    start = 0
    for i in range(len(data) - 1):
        row = data[i]
        time = row[1]
        if version == kinect.KinectVersion.FIRST:
            if float(row[62]) == 1 and start == 0:  # 62 column for standing up phase flag
                start = kinect.get_seconds(time)
            if float(row[67]) == 1 and float(
                    data[i + 1][67]) != 1 and start != 0:  # 67 column for final phase of the experiment
                stop = kinect.get_seconds(time)
                durations.append(abs(stop - start))
                start = 0
        else:
            raise NotImplementedError
    return durations


class Walk:
    def __init__(self):
        self.bodies = []

    def add_frame(self, frame, version):
        body = kinect.Body.from_raw_data(frame, version)
        self.bodies.append(body)

    def calculate_distances_between_ankles(self):
        distances = []
        for i in range(len(self.bodies)):
            body = self.bodies[i]
            distances.append(body.calc_distance_between_ankles())
        return distances

    def convert_coordinates_to_relative(self):
        new_bodies = []
        for body in self.bodies:
            new_bodies.append(kinect.Body.with_relative_coordinates(body))
        self.bodies = new_bodies

    def detect_steps(self):  # TODO add first step recognition, not minima, but from minimum value
        step_indices = []
        distances = self.calculate_distances_between_ankles()
        smoothed = [0] + parameters.smooth_using_moving_average(3, distances)
        abs_smoothed = []
        for dist in smoothed:
            abs_smoothed.append(abs(dist))
        indices = argrelextrema(np.array(abs_smoothed), np.less)[0]
        for i in range(len(indices)):
            index = indices[i]
            if i + 1 == len(indices):
                last = len(abs_smoothed) - 1
            else:
                last = indices[i + 1]
            if abs_smoothed[index] < 0.07 and (max(abs_smoothed[index:last]) > 0.12 or (
                    i == len(indices) - 1 and max(abs_smoothed[max(0, i - 1):index]) > 0.12)):
                step_indices.append(index)
        steps = []
        # self.convert_coordinates_to_relative()
        if len(step_indices) > 1:
            for i in range(1, len(step_indices)):
                steps.append(self.bodies[step_indices[i - 1]: step_indices[i] + 1])
        # draw.draw_steps(distances, abs_smoothed, step_indices)
        return steps
