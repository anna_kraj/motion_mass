import matplotlib.pyplot as plt

from kinect import JointType


def draw_chart(distances):
    plt.axhline(0, color='black')
    plt.plot(distances)
    plt.show(block=True)


def draw_two_charts(data, data2):
    plt.axhline(0, color='black')
    plt.xlabel('frame')
    plt.ylabel('distance')
    plt.plot(data)
    plt.plot(data2, color='red')
    plt.show(block=True)


def draw_steps(distances, abs_smoothed, steps):
    plt.axhline(0, color='black')
    plt.xlabel('frame')
    plt.ylabel('distance')
    plt.plot(distances)
    plt.plot(abs_smoothed, color='red')
    for step in steps:
        plt.axvline(step, color='green')
    plt.show(block=True)


def plot_bone(joints, joint1, joint2):
    plt.plot([joints[joint1].z, joints[joint2].z], [joints[joint1].y, joints[joint2].y])


def draw_body(body):
    joints = body.joints

    plot_bone(joints, JointType.Head, JointType.SpineShoulder)
    plot_bone(joints, JointType.SpineShoulder, JointType.SpineMid)
    plot_bone(joints, JointType.SpineMid, JointType.SpineBase)
    plot_bone(joints, JointType.SpineShoulder, JointType.ShoulderRight)
    plot_bone(joints, JointType.SpineShoulder, JointType.ShoulderLeft)
    plot_bone(joints, JointType.SpineBase, JointType.HipRight)
    plot_bone(joints, JointType.SpineBase, JointType.HipLeft)

    plot_bone(joints, JointType.ShoulderRight, JointType.ElbowRight)
    plot_bone(joints, JointType.ElbowRight, JointType.WristRight)
    plot_bone(joints, JointType.WristRight, JointType.HandRight)

    plot_bone(joints, JointType.ShoulderLeft, JointType.ElbowLeft)
    plot_bone(joints, JointType.ElbowLeft, JointType.WristLeft)
    plot_bone(joints, JointType.WristLeft, JointType.HandLeft)

    plot_bone(joints, JointType.HipRight, JointType.KneeRight)
    plot_bone(joints, JointType.KneeRight, JointType.AnkleRight)
    plot_bone(joints, JointType.AnkleRight, JointType.FootRight)

    plot_bone(joints, JointType.HipLeft, JointType.KneeLeft)
    plot_bone(joints, JointType.KneeLeft, JointType.AnkleLeft)
    plot_bone(joints, JointType.AnkleLeft, JointType.FootLeft)

    plt.axes().set_aspect('equal', 'datalim')
    plt.axes().get_xaxis().set_visible(False)
    plt.axes().get_yaxis().set_visible(False)
    plt.gca().invert_xaxis()
    # plt.show()


def draw_body_points(body):
    draw_body(body)
    for joint in body.joints.values():
        plt.plot([joint.z], [joint.y], 'go')
    plt.axes().set_aspect('equal', 'datalim')
    plt.axes().get_xaxis().set_visible(False)
    plt.axes().get_yaxis().set_visible(False)
    plt.gca().invert_xaxis()
    plt.show()
